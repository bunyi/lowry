package server

import (
	"testing"
)

func TestValidUserName(t *testing.T) {
	if validUserName("a") {
		t.Errorf("Got valid user for single char user")
	}
	if validUserName("bo") {
		t.Errorf("Got valid user for less than 3 char user")
	}
	if validUserName("-aaksjkj") {
		t.Errorf("Got valid user for a user that starts with '-'")
	}
	if validUserName("_zoz") {
		t.Errorf("Got valid user for a user that starts with '_'")
	}
	if validUserName("76aa+o") {
		t.Errorf("Got valid user for a user that contains '+'")
	}

	if !validUserName("name") {
		t.Errorf("Got invalid user for a good name")
	}
}
