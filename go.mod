module git.sindominio.net/sindominio/lowry

go 1.12

require (
	code.gitea.io/sdk/gitea v0.11.3
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-asn1-ber/asn1-ber v1.4.1 // indirect
	github.com/go-ldap/ldap/v3 v3.1.8
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/namsral/flag v1.7.4-pre
	go.etcd.io/bbolt v1.3.4
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
