all: js go

js:
	npm install
	npm install --save-dev expose-loader
	npm run build
	mkdir -p dist/js
	cp assets/zxcvbn.js dist/js
	cp assets/zxcvbn-bootstrap-strength-meter.js dist/js
	cp assets/jquery.dynatable* dist/js

go:
	go get .
	go build

clean:
	rm -rf node_modules dist lowry

deps:
	sudo apt install slapd ldap-utils golang npm

fixtures:
	sudo cp examples/sindominio.* /etc/ldap/schema/
	sudo ldapadd -Y EXTERNAL -H ldapi:// -f /etc/ldap/schema/sindominio.ldif
	sudo slapadd -n 1 -l examples/data.ldif

demo:
	./lowry -config examples/lowry.conf

test:
	go test ./...
