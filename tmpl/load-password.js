<script>
window.onload = function() {
  $(document).ready(function() {
    $("#StrengthProgressBar").zxcvbnProgressBar({
                              passwordInput: "#password",
			      ratings: ["Muy d&eacute;bil", "D&eacute;bil", "Casi buena!", "Fuerte", "Muy buena!"]
                              });
  });

  $(document).ready(function() {
    $('#needs-validation').addEventListener("submit", function(event) {
      if (form.password.value !== form.password2.value) {
        event.preventDefault();
        event.stopPropagation();
	$('#password2').add("is-invalid");
      } else {
	$('#password').password.add("was-validated");
	$('#password2').password2.add("was-validated");
      }
    }, false);
  });
}
</script>
