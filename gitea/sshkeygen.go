package gitea

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"golang.org/x/crypto/ssh"
)

func GenerateRSAKeyPair(bits int) (userkeypriv, userkeypub string, err error) {
	// generate private key pair, private key with pem format and public key with authorized_keys format
	var privateKey *rsa.PrivateKey
	privateKey, _ = rsa.GenerateKey(rand.Reader, bits)
	privateKeyDer := x509.MarshalPKCS1PrivateKey(privateKey)
	privateKeyBlock := pem.Block{
		Type:    "RSA PRIVATE KEY",
		Headers: nil,
		Bytes:   privateKeyDer,
	}
	privateKeyPem := pem.EncodeToMemory(&privateKeyBlock)

	publicKey := privateKey.PublicKey
	pub, _ := ssh.NewPublicKey(&publicKey)

	userkeypriv = string(privateKeyPem)
	userkeypub = string(ssh.MarshalAuthorizedKey(pub))
	return
}
