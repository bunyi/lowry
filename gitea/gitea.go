package gitea

import (
	"code.gitea.io/sdk/gitea"
	"fmt"
	"log"
	"net/url" // to check URL init vars
	"reflect" // to compare structs
)

// Gitea holds the gitea functionality config vars
type Gitea struct {
	giteaURL          string
	token             string
	cloneAddr         string
	webhookRepoURL    string
	webhookRepoSecret string
	webhookURL        string
	webhookSecret     string
}

type Repo struct {
	user              string
	repo              string
	cloneAddr         string
	webhookRepoURL    string
	webhookRepoSecret string
	client            *gitea.Client
}

func isValidURL(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	return err == nil
}

// Init gitea config vars
func Init(giteaURL, token, cloneAddr, webhookRepoURL, webhookRepoSecret, webhookURL, webhookSecret string) *Gitea {
	// check URL values
	if !isValidURL(giteaURL) || !isValidURL(cloneAddr) || !isValidURL(webhookRepoURL) || !isValidURL(webhookURL) {
		giteaURL = "" // to avoid process
		log.Println("Some config URL fields are not valid.")
	}

	return &Gitea{
		giteaURL:          giteaURL,
		token:             token,
		cloneAddr:         cloneAddr,
		webhookRepoURL:    webhookRepoURL,
		webhookRepoSecret: webhookRepoSecret,
		webhookURL:        webhookURL,
		webhookSecret:     webhookSecret,
	}

}

func (g *Gitea) Repo(user string, repo string) (*Repo, error) {
	if g.giteaURL == "" || g.token == "" || g.webhookRepoSecret == "" || g.webhookSecret == "" {
		return nil, fmt.Errorf("Gitea config isn't right, some fields are empty.")
	}
	return &Repo{
		user:              user,
		repo:              repo,
		cloneAddr:         g.cloneAddr,
		webhookRepoURL:    g.webhookRepoURL,
		webhookRepoSecret: g.webhookRepoSecret,
		client:            gitea.NewClient(g.giteaURL, g.token),
	}, nil
}

// UserID check users exists and return user id
func (r *Repo) UserID() (int, error) {
	userexist, err := r.client.SearchUsers(r.user, 1)
	if err != nil {
		return -1, err
	}

	emptyuser := []*gitea.User{}
	if reflect.DeepEqual(userexist, emptyuser) == true {
		return -2, err
	}
	uid := int(userexist[0].ID)
	return uid, nil
}

// Exists checks if the repo exists.
func (r *Repo) Exists() bool {
	repoexists, err := r.client.GetRepo(r.user, r.repo)
	if err != nil && err.Error() != "404 Not Found" {
		return false
	}
	// (API responses with empty repo if doesn't exist.)
	emptyrepo := &gitea.Repository{}
	return !reflect.DeepEqual(repoexists, emptyrepo)
}

// Delete the repo
func (r *Repo) Delete() error {
	return r.client.DeleteRepo(r.user, r.repo)
}

// Migrate template repo to new user repo
func (r *Repo) Migrate() error {
	uid, err := r.UserID()
	if err != nil {
		return err
	}

	migrateRepoOption := gitea.MigrateRepoOption{
		CloneAddr:   r.cloneAddr,
		Description: "Basic hugo site repo",
		UID:         uid,
		RepoName:    r.repo,
		Private:     true}

	_, err = r.client.MigrateRepo(migrateRepoOption)
	return err
}

// DeployKey put a deploy key on the user repo
func (r *Repo) DeployKey(keypub string) error {
	deployKeyOption := gitea.CreateKeyOption{
		Key:      keypub,
		ReadOnly: true,
		Title:    "Web deploy key"}
	_, err := r.client.CreateDeployKey(r.user, r.repo, deployKeyOption)
	if err != nil {
		return err
	}
	return nil
}

// CreateWebhook create a webhook on the user repo
func (r *Repo) CreateWebhook() error {
	webHookOption := gitea.CreateHookOption{
		Active: true,
		Config: map[string]string{"content_type": "json",
			"url":    r.webhookRepoURL,
			"secret": r.webhookRepoSecret},
		Events: []string{"push"},
		Type:   "gitea"}

	_, err := r.client.CreateRepoHook(r.user, r.repo, webHookOption)
	// TODO: <nil> no vale pa na
	if err != nil && err.Error() != "<nil>" {
		log.Printf("API Error on migrate.")
		return err
	}
	return nil

}
// Get repo info
func (r *Repo) GetSSHURL() (string, error) {
	createdRepo, err := r.client.GetRepo(r.user, r.repo)
	// TODO: <nil> no vale pa na
	if err != nil {
		log.Printf("Error on getting info from new created Repo.")
		return "", err
	}
	return createdRepo.SSHURL, nil

}
