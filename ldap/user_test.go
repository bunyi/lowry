package ldap

import "testing"

const (
	user     = "user"
	userPass = "foobar"
	newPass  = "newpass"
)

func TestValidate(t *testing.T) {
	l := testLdap(t)
	err := l.ValidateUser(user, userPass)
	if err != nil {
		t.Errorf("Error on ValidateUser(): %v", err)
	}
}

func TestValidateFails(t *testing.T) {
	l := testLdap(t)
	err := l.ValidateUser(user, userPass+"bar")
	if err == nil {
		t.Errorf("ValidateUser() didn't fail to auth the user")
	}
}

func TestChangePass(t *testing.T) {
	l := testLdap(t)
	err := l.ChangePass(user, userPass, newPass)
	if err != nil {
		t.Errorf("Error on ChangePass(): %v", err)
	}

	err = l.ValidateUser(user, newPass)
	if err != nil {
		t.Errorf("Error on ValidateUser(): %v", err)
	}

	err = l.ChangePass(user, newPass, userPass)
	if err != nil {
		t.Errorf("Error on the second ChangePass(): %v", err)
	}
}

func TestChangePassRO(t *testing.T) {
	l := testLdap(t)
	l.RO = true
	err := l.ChangePass(user, userPass, newPass)
	if err != nil {
		t.Errorf("Error on ChangePass(): %v", err)
	}

	err = l.ValidateUser(user, newPass)
	if err == nil {
		t.Errorf("ValidateUser() didn't fail to auth the user")
	}
}

func TestChangePassAdmin(t *testing.T) {
	l := testLdap(t)
	err := l.ChangePassAdmin(user, newPass)
	if err != nil {
		t.Fatalf("Error on ChangePassAdmin(): %v", err)
	}

	err = l.ValidateUser(user, newPass)
	if err != nil {
		t.Errorf("Error on ValidateUser(): %v", err)
	}

	err = l.ChangePassAdmin(user, userPass)
	if err != nil {
		t.Errorf("Error on the second ChangePassAdmin(): %v", err)
	}
}

func TestGetUser(t *testing.T) {
	l := testLdap(t)

	user, err := l.GetUser("user")
	if err != nil {
		t.Errorf("GetUsers(\"user\") failed: %v", err)
	}
	if user.UID != 1000 {
		t.Errorf("Expected 1000 found: %v", user.UID)
	}
	if user.Shell != "/bin/bash" {
		t.Errorf("Expected /bin/bash found: %v", user.Shell)
	}

	user, err = l.GetUser("superuser")
	if err != nil {
		t.Errorf("GetUsers(\"superuser\") failed: %v", err)
	}
	if user.UID != 1001 {
		t.Errorf("Expected 1001 found: %v", user.UID)
	}
	if user.Shell != "/bin/bash" {
		t.Errorf("Expected /bin/bash found: %v", user.Shell)
	}
}

func TestListUsers(t *testing.T) {
	l := testLdap(t)
	userCount := 0
	users, err := l.ListUsers()
	if err != nil {
		t.Errorf("ListUsers() failed: %v", err)
	}
	for _, user := range users {
		if user.Name == "user" {
			userCount++
			if user.UID != 1000 {
				t.Errorf("Expected 1000 found: %v", user.UID)
			}
			if user.Shell != "/bin/bash" {
				t.Errorf("Expected /bin/bash found: %v", user.Shell)
			}
		}
		if user.Name == "superuser" {
			userCount++
			if user.UID != 1001 {
				t.Errorf("Expected 1001 found: %v", user.UID)
			}
			if user.Shell != "/bin/bash" {
				t.Errorf("Expected /bin/bash found: %v", user.Shell)
			}
		}
	}
	if userCount != 2 {
		t.Errorf("Not the righ number of users: %v", users)
	}
}

func TestAddUser(t *testing.T) {
	const newUser = "newUser"
	l := testLdap(t)
	err := l.AddUser(newUser, newPass, 0)
	if err != nil {
		t.Errorf("Error on AddUser(): %v", err)
	}

	err = l.ValidateUser(newUser, newPass)
	if err != nil {
		t.Errorf("Error on ValidateUser(): %v", err)
	}

	err = l.DelUser(newUser)
	if err != nil {
		t.Errorf("Error on DelUser(): %v", err)
	}

}

func TestAddExistingUser(t *testing.T) {
	l := testLdap(t)
	err := l.AddUser(user, newPass, 0)
	if err == nil {
		t.Errorf("It was possible to create an already existing user")
	}

}

func TestChangeShell(t *testing.T) {
	const shell = "otherShell"
	l := testLdap(t)

	user, err := l.GetUser("user")
	actualShell := user.Shell

	err = l.ChangeShell("user", shell)
	if err != nil {
		t.Errorf("ChangeShell() failed: %v", err)
	}
	user, err = l.GetUser("user")
	if user.Shell != shell {
		t.Errorf("The shell was not changed: %v", user.Shell)
	}

	err = l.ChangeShell("user", actualShell)
	if err != nil {
		t.Errorf("ChangeShell() failed: %v", err)
	}
	user, err = l.GetUser("user")
	if user.Shell != actualShell {
		t.Errorf("The shell was not changed: %v", user.Shell)
	}
}
